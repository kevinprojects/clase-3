//
//  ArsToBtcViewController.swift
//  MiPrimeraApp
//
//  Created by Kevin Belter on 11/8/16.
//  Copyright © 2016 Kevin Belter. All rights reserved.
//

import UIKit

class ArsToBtcViewController: UIViewController {

    @IBOutlet weak var txtValor: UITextField!
    @IBOutlet weak var lblResultado: UILabel!
    
    @IBAction func tocoConvertir(_ sender: UIButton) {
        //Se va llamar cuando la persona toque convertir.
        
        let valorBTC = 0.000094
        
        if let valorString = txtValor.text, let valor = Double(valorString)  {
            let resultado = valor * valorBTC
            lblResultado.text = "\(resultado)"
        }
        
        txtValor.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
